import React, { useEffect, useState } from "react";
import Slider from "react-slick";
import Heading from "../Products/Heading";
import Product from "../Products/Product";
import { newArrOne, newArrTwo, newArrThree, newArrFour } from "../../../assets/images/index";
import SampleNextArrow from "./SampleNextArrow";
import SamplePrevArrow from "./SamplePrevArrow";
import { ProductAPI } from "../../../apis/ProductAPI";

const NewArrivals = () => {
   const settings = {
      infinite: true,
      speed: 500,
      slidesToShow: 4,
      slidesToScroll: 1,
      nextArrow: <SampleNextArrow />,
      prevArrow: <SamplePrevArrow />,
      responsive: [
         {
            breakpoint: 1025,
            settings: {
               slidesToShow: 3,
               slidesToScroll: 1,
               infinite: true,
            },
         },
         {
            breakpoint: 769,
            settings: {
               slidesToShow: 2,
               slidesToScroll: 2,
               infinite: true,
            },
         },
         {
            breakpoint: 480,
            settings: {
               slidesToShow: 1,
               slidesToScroll: 1,
               infinite: true,
            },
         },
      ],
   };

   const [products, setProducts] = useState([]);

   const getProducts = async () => {
      const { data } = await ProductAPI.newArrivals();
      setProducts(data);
   };

   useEffect(() => {
      getProducts();
   }, []);
   return (
      <div className="w-full pb-16">
         <Heading heading="New Arrivals" />
         <Slider {...settings}>
            {products?.map((item) => (
               <div key={item?.id} className="px-2">
                  <Product
                     _id={item?.id}
                     img={item?.imageUrl}
                     productName={item?.title || "Round Table Clock"}
                     price={item?.price}
                     color={item?.color}
                     badge={true}
                     des={item?.description}
                     category={item?.category?.name}
                  />
               </div>
            ))}

            {/* <div className="px-2">
               <Product
                  _id="100002"
                  img={newArrTwo}
                  productName="Smart Watch"
                  price="250.00"
                  color="Black"
                  badge={true}
                  des="Lorem ipsum dolor sit amet consectetur adipisicing elit. Hic excepturi quibusdam odio deleniti reprehenderit facilis."
               />
            </div>
            */}
         </Slider>
      </div>
   );
};

export default NewArrivals;

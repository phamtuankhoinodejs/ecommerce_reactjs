import axios from "axios";
const path = "api/cart/";

export const CartAPI = {
   async addCartItem(payload) {
      return await axios.post(path + "add-item", payload);
   },
};
